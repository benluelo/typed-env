use std::collections::HashMap;

use heck::SnakeCase;
use proc_macro::TokenStream;
use proc_macro2::Ident;
use quote::{quote, ToTokens};
use syn::{parse_macro_input, spanned::Spanned, DeriveInput, ItemMod, ItemStruct};

#[proc_macro_derive(EnvVar)]
pub fn env_var_derive(token_stream: TokenStream) -> TokenStream {
    let _item_mod = parse_macro_input!(token_stream as DeriveInput);

    todo!()
}

#[proc_macro_attribute]
pub fn typed_env(_attributes_token_stream: TokenStream, token_stream: TokenStream) -> TokenStream {
    let item_mod = parse_macro_input!(token_stream as ItemMod);

    let mod_name = item_mod.ident.clone();

    // TODO: use with_capacity (or iter.map.colllect)
    let mut all_items = HashMap::<Ident, Ident>::new();

    let item_mod_content = match &item_mod.content {
        Some((_, content)) => content,
        None => {
            return syn::parse::Error::new_spanned(item_mod, "module content is required")
                .to_compile_error()
                .into();
        }
    };

    for item in item_mod_content {
        let item_tokens = item.to_token_stream().into();
        let struct_item = parse_macro_input!(item_tokens as ItemStruct);

        all_items.insert(
            Ident::new(
                &struct_item.ident.to_string().to_snake_case(),
                struct_item.span(),
            ),
            struct_item.ident,
        );
    }

    let env_fields = all_items.iter().map(|(field_ident, type_ident)| {
        quote! { pub(crate) #field_ident: #type_ident }
    });

    let env_struct = quote! {
        #[non_exhaustive]
        pub(crate) struct Env {
            #(#env_fields),*
        }
    };

    let env_builder_fields = all_items.iter().map(|(field_ident, type_ident)| {
        quote! { #field_ident: Option<#type_ident> }
    });

    let env_builder_init_fields = all_items.iter().map(|(field_ident, type_ident)| {
        quote! { #field_ident: <#type_ident as ::typed_env::EnvVar>::from_env(&mut errors) }
    });

    let env_builder_pattern_fields = all_items.keys().map(|field_ident| {
        quote! { #field_ident: Some(#field_ident) }
    });

    let fields = all_items.keys();

    let read_env = quote! {
        pub(crate) fn read_env() -> Env {
            pub struct EnvBuilder {
                #(#env_builder_fields),*
            }

            let mut errors = String::new();

            let builder = EnvBuilder {
                #(#env_builder_init_fields),*
            };

            if let EnvBuilder {
                #(#env_builder_pattern_fields),*
            } = builder {
                return Env {
                    #(#fields),*
                };
            }

            eprintln!("Environment variable errors:\n{}", errors);

            std::process::exit(-1)
        }
    };

    // let content = item_mod.content.unwrap();

    (quote! {
        pub(crate) mod #mod_name {
            #(#item_mod_content)*

        use std::fmt::Write;

        #env_struct

        #read_env
        }
    })
    .into()
}
