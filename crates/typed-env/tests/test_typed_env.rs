use typed_env::{typed_env, EnvVar};

use crate::vars::{RedditClientId, SomeId};

impl EnvVar for RedditClientId {
    const NAME: &'static str = "REDDIT_CLIENT_ID";

    type Inner = String;

    fn try_from_string(inner: String) -> Result<Self, Option<String>> {
        str::parse::<Self::Inner>(&inner)
            .map(Self)
            .map_err(|why| Some(why.to_string()))
    }

    fn get(&self) -> &Self::Inner {
        &self.0
    }
}
impl EnvVar for SomeId {
    const NAME: &'static str = "SOME_ID";

    type Inner = u32;

    fn try_from_string(inner: String) -> Result<Self, Option<String>> {
        str::parse::<Self::Inner>(&inner)
            .map(Self)
            .map_err(|why| Some(why.to_string()))
    }

    fn get(&self) -> &Self::Inner {
        &self.0
    }
}

#[typed_env]
mod vars {
    //     use super::EnvVar;
    //     use iri_string::types::UriAbsoluteString;
    //     use twilight_model::id::GuildId;

    #[derive(Debug /* EnvVar */)]
    pub(crate) struct RedditClientId(pub String);

    //     #[derive(Debug, EnvVar)]
    //     pub(crate) struct RedditClientSecret(String);

    //     impl EnvVar for RedditClientSecret {
    //         const NAME: &'static str = "REDDIT_CLIENT_SECRET";

    //         type Inner = String;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct TcgcollectorGuildId(GuildId);

    #[derive(Debug)]
    pub(crate) struct SomeId(pub u32);

    //     impl EnvVar for TcgcollectorGuildId {
    //         const NAME: &'static str = "TCGCOLLECTOR_DISCORD_GUILD_ID";

    //         type Inner = GuildId;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<u64>(&inner)
    //                 .map(Self::Inner::from)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct TcgcollectorUsername(String);

    //     impl EnvVar for TcgcollectorUsername {
    //         const NAME: &'static str = "TCGCOLLECTOR_USERNAME";

    //         type Inner = String;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct TcgcollectorPassword(String);

    //     impl EnvVar for TcgcollectorPassword {
    //         const NAME: &'static str = "TCGCOLLECTOR_PASSWORD";

    //         type Inner = String;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct TcgcollectorBasePath(UriAbsoluteString);

    //     impl EnvVar for TcgcollectorBasePath {
    //         const NAME: &'static str = "TCGCOLLECTOR_BASE_PATH";

    //         type Inner = UriAbsoluteString;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct DiscordToken(String);

    //     impl EnvVar for DiscordToken {
    //         const NAME: &'static str = "DISCORD_TOKEN";

    //         type Inner = String;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }

    //     #[derive(Debug)]
    //     pub(crate) struct DatabaseUrl(UriAbsoluteString);

    //     impl EnvVar for DatabaseUrl {
    //         const NAME: &'static str = "DATABASE_URL";

    //         type Inner = UriAbsoluteString;

    //         fn try_from_string(inner: String) -> Result<Self, Option<String>> {
    //             str::parse::<Self::Inner>(&inner)
    //                 .map(Self)
    //                 .map_err(|_| None)
    //         }

    //         fn get(&self) -> Self::Inner {
    //             self.0.clone()
    //         }
    //     }
}

#[test]
fn test_typed_env() {
    let vars = vars::read_env();

    let _secret = vars.reddit_client_id.get();
}
