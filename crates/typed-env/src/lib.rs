pub use typed_env_macros::*;

/// Type-safe environment variables.
///
/// ```rust,norun
/// #[derive(EnvVar)]
/// #[env_var(...)]
/// struct RetryCount(u8);
/// ```
///
/// ## Renaming
///
/// The name of the struct would be used as the environment variable, translated to
/// `SCREAMING_SNAKE_CASE`, unless otherwise specified using `rename`.
///
/// ## Custom Parsing
///
/// `parse`: A function with the signature `fn(String) -> Self::Inner`, to be used to parse the value
/// of the environment variable into it's domain type.
///
/// ### Or...
///
/// `intermediary_type`: The type to parse to first before wrapping it with [`EnvVar::Inner`], using
/// [`EnvVar::Inner`]'s [`From<intermediary_type>`] implementation.
/// `wrap_fn`: The function to use if [`EnvVar::Inner`] doesn't implement [`From<intermediary_type>`]
///
/// Note that `parse` is incompatable with `intermediary_type` and `wrap_fn`.
///
/// # Examples
///
/// ```rust
/// #[derive(EnvVar)]
/// #[env_var(intermediary_type = "u64")]
/// pub(crate) struct TcgcollectorGuildId(GuildId);
/// ```
///
/// Would generate the following:
///
/// ```rust
/// impl EnvVar for TcgcollectorGuildId {
///     const NAME: &'static str = "TCGCOLLECTOR_DISCORD_GUILD_ID";
///     
///     type Inner = GuildId;
///     
///     fn try_from_string(inner: String) -> Result<Self, Option<String>> {
///         str::parse::<u64>(&inner)
///             .map(Self::Inner::from)
///             .map(Self)
///             .map_err(|_| None)
///     }
///     
///     fn get(&self) -> Self::Inner {
///         self.0
///     }
/// }
/// ```
///
/// I see no reason why this would be useful on any struct types other than newtypes.
pub trait EnvVar: Sized {
    /// The name of the environment variable.
    const NAME: &'static str;

    /// The inner type that the environment variable wraps.
    ///
    /// A variable called `RETRY_COUNT` may be a `u8`, for example, so it would wrap a `u8`.
    type Inner;

    /// Attempts to retrieve the value of the environment variable from the environment.
    ///
    /// # Errors
    /// This function will return `Some(t)` when the value is present and can be successfully
    /// parsed, otherwise it will write an error to the provided [`Write`] buffer and return `None`.
    fn from_env(writer: &mut impl std::fmt::Write) -> Option<Self> {
        #[allow(clippy::disallowed_method)]
        if let Ok(value) = std::env::var(Self::NAME) {
            Some(match <Self as EnvVar>::try_from_string(value) {
                Ok(ok) => ok,
                Err(message) => {
                    let _ = writeln!(
                        writer,
                        "{} could not be parsed as a `{}`.",
                        Self::NAME,
                        std::any::type_name::<Self::Inner>(),
                    );
                    if let Some(message) = message {
                        let _ = writeln!(writer, "  message: {}", message);
                    }
                    return None;
                }
            })
        } else {
            let _ = writeln!(writer, "{} not present.", Self::NAME);
            None
        }
    }

    /// Tries to parse the value of the environment variable from it's [`String`] value.
    ///
    /// You should prefer implementing this function over [`EnvVar::from_env`] as `from_env` is implemented
    /// using [`EnvVar::try_from_string`].
    ///
    /// # Errors
    /// This function will error if the provided value cannot be parsed into the underlying
    /// [`EnvVar::Inner`] type, supplying an optional message as to why.
    fn try_from_string(inner: String) -> Result<Self, Option<String>>;

    /// Returns the value of the environment variable.
    fn get(&self) -> &Self::Inner;
}
